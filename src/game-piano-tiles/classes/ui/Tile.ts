import JsonKey from '../data/JsonKey';

export default class Tile extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  onComplete: Phaser.Signal;

  onDown: Phaser.Signal;

  private _backImage: Phaser.Image;
  
  private _frontImage: Phaser.Image;

  private _column: number;
  
  private _row: number;

  private _isTarget: boolean;

  private _isEnabled: boolean;

  private _isAvailable: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, parent: PIXI.DisplayObjectContainer, x: number, y: number,
  column: number, row: number) {
    super(game, parent);
    
    this.onComplete = new Phaser.Signal();
    this.onDown = new Phaser.Signal();

    this.x = x;
    this.y = y;
    this._column = column;
    this._row = row;

    this._isEnabled = false;
    this._isAvailable = false;

    this.initImages();

    this.setAsTrap();
  }

  // ----------------------------------------------------------------------------------------------
  get column(): number {
    return (this._column);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.onComplete.dispose();
    this.onDown.dispose();
    super.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  get isAvailable(): boolean {
    return (this._isAvailable);
  }

  // ----------------------------------------------------------------------------------------------
  get row(): number {
    return (this._row);
  }

  // ----------------------------------------------------------------------------------------------
  setAsTarget(): void {
    this._frontImage.frameName = 'black_tile';
    this._frontImage.alpha = 1.0;
    this._isTarget = true;
    this._isEnabled = true;
    this._isAvailable = false;
  }

  // ----------------------------------------------------------------------------------------------
  setAsTrap(): void {
    this._frontImage.frameName = 'white_tile';
    this._frontImage.alpha = 0;
    this._isTarget = false;
    this._isEnabled = true;
    this._isAvailable = true;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private initImages(): void {
    this._backImage = this.game.add.image(0, 0, JsonKey.GRAPHICS_IMAGES, 'white_tile', this);
    
    this._frontImage = this.game.add.image(0, 0, JsonKey.GRAPHICS_IMAGES, 'white_tile', this);
    this._frontImage.inputEnabled = true;
    this._frontImage.events.onInputDown.add(this.onInputDown, this);

    this._frontImage.alpha = 0;
  }

  // ----------------------------------------------------------------------------------------------
  private onInputDown(): void {
    if (!this._isEnabled) {
      return;
    }

    this._isEnabled = false;
    
    if (this._isTarget) {
      this.presentCorrect();
    } else {
      this.presentIncorrect();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private present(frameName: string): void {
    this._frontImage.frameName = frameName;
    this._frontImage.alpha = 1;
    
    const DURATION = 400;
    const AUTO_START = true;
    const tween = this.game.tweens.create(this._frontImage).to(
      {
        alpha: 0,
      },
      DURATION,
      Phaser.Easing.Linear.None,
      AUTO_START);

    tween.onComplete.addOnce(() => {
      this._isAvailable = true;

      if (this._isTarget) {
        this._isTarget = false;
        this.onComplete.dispatch(this);
      }

      if (!this._isTarget) {
        this.setAsTrap();
      }
    }, this);
  }

  // ----------------------------------------------------------------------------------------------
  private presentCorrect(): void {
    this.present('green_tile');
    const IS_CORRECT = true;
    this.onDown.dispatch(this, IS_CORRECT);
  }

  // ----------------------------------------------------------------------------------------------
  private presentIncorrect(): void {
    this.present('red_tile');
    const IS_CORRECT = false;
    this.onDown.dispatch(this, IS_CORRECT);
  }
}
