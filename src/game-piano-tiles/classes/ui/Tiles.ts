import Tile from './Tile';

export default class Tiles extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly NUM_COLS = 4;

  private static readonly NUM_ROWS = 4;

  private static readonly TILE_X_START = 4;

  private static readonly TILE_Y_START = 4;

  private static readonly TILE_X_OFFSET = 4;
  
  private static readonly TILE_Y_OFFSET = 4;

  onTileComplete: Phaser.Signal;

  onTileDown: Phaser.Signal;

  private _tiles: Tile[];

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    super(game)

    this.onTileComplete = new Phaser.Signal();
    this.onTileDown = new Phaser.Signal();

    this.createTiles();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.onTileComplete.dispose();
    this.onTileDown.dispose();
  }

  // ----------------------------------------------------------------------------------------------
  setRandomTilesAsTargets(numberOfTiles: number): void {
    const availableTiles = Phaser.ArrayUtils.shuffle(this.getAvailableTiles());
    numberOfTiles = Phaser.Math.clamp(numberOfTiles, 0, availableTiles.length);

    while (numberOfTiles > 0) {
      const tile = availableTiles.pop();
      if (tile) {
        tile.setAsTarget();
      }

      numberOfTiles -= 1;
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private createTile(x: number, y: number, column: number, row: number): Tile {
    const tile = new Tile(this.game, this, x, y, column, row);
    
    tile.onComplete.add((target: Tile) => {
      this.onTileComplete.dispatch(target, this);
    }, this);
    
    tile.onDown.add((target: Tile, isCorrect: boolean) => {
      this.onTileDown.dispatch(target, isCorrect, this);
    }, this);

    return (tile);
  }

  // ----------------------------------------------------------------------------------------------
  private createTiles(): void {
    this._tiles = [];
    let tile: Tile;

    let y = Tiles.TILE_Y_START;
    for (let row = 0; row < Tiles.NUM_ROWS; row += 1) {
      let x = Tiles.TILE_X_START;
      for (let column = 0; column < Tiles.NUM_COLS; column += 1) {
        tile = this.createTile(x, y, column, row)
        this._tiles.push(tile);
        x += tile.width + Tiles.TILE_X_OFFSET;
      }

      if (tile) {
        y += tile.height + Tiles.TILE_Y_OFFSET;
      }
    }
  }

  // ----------------------------------------------------------------------------------------------
  private getAvailableTiles(): Tile[] {
    const out: Tile[] = [];
    
    this._tiles.forEach((tile) => {
      if (tile.isAvailable) {
        out.push(tile);
      }
    });

    return (out);
  }
}
