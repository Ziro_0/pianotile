import IPianoTilesGameConfig from '../../IPianoTilesGameConfig';
import { getBooleanProperty, getNumberProperty, getStringProperty } from '../../util/util';
import PianoGameDefaults from './PianoGameDefaults';

// ------------------------------------------------------------------------------------------------
export default function pianoConfigValidate(config: IPianoTilesGameConfig): IPianoTilesGameConfig {
  if (!config) {
    config = {};
  }

  config.enduranceBonusTime = getNumberProperty(config, 'enduranceBonusTime',
    PianoGameDefaults.ENDURANCE_BONUS_TIME);

  config.enduranceNumTiles = getNumberProperty(config, 'enduranceNumTiles',
    PianoGameDefaults.ENDURANCE_NUM_TILES);

  config.enduranceNumTilesTimeBonus = getNumberProperty(config, 'enduranceNumTilesTimeBonus',
    PianoGameDefaults.ENDURANCE_NUM_TILES_TIME_BONUS);

  config.enduranceTime = getNumberProperty(config, 'enduranceNumTime',
    PianoGameDefaults.ENDURANCE_TIME);

  config.frenzyIncrMaxTileRate = getNumberProperty(config, 'frenzyIncrMaxTileRate',
    PianoGameDefaults.FRENZY_INCR_MAX_TILES_RATE);

  config.frenzyNumMaxTiles = getNumberProperty(config, 'frenzyNumMaxTiles',
    PianoGameDefaults.FRENZY_NUM_MAX_TILES);

  config.frenzyNumStartTiles = getNumberProperty(config, 'frenzyNumStartTiles',
    PianoGameDefaults.FRENZY_NUM_START_TILES);

  config.frenzyTime = getNumberProperty(config, 'frenzyTime', PianoGameDefaults.FRENZY_TIME);

  config.gameMode = getStringProperty(config, 'gameMode', PianoGameDefaults.GAME_MODE);

  config.lostLifeNumFlashes = getNumberProperty(config, 'lostLifeNumFlashes',
    PianoGameDefaults.LOST_LIFE_NUM_FLASHES);

  config.lostLastLifeNumFlashes = getNumberProperty(config, 'lostLastLifeNumFlashes',
    PianoGameDefaults.LOST_LAST_LIFE_NUM_FLASHES);

  config.patternNumTiles = getNumberProperty(config, 'patternNumTiles',
    PianoGameDefaults.PATTERN_NUM_TILES);

  config.patternTime = getNumberProperty(config, 'patternTime', PianoGameDefaults.PATTERN_TIME);

  config.shouldScreenShake = getBooleanProperty(config, 'shouldScreenShake',
    PianoGameDefaults.SHOULD_SCREEN_SHAKE);

  config.showTutorial = getBooleanProperty(config, 'showTutorial',
    PianoGameDefaults.SHOW_TUTORIAL);

  return (config);
}
