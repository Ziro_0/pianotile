import Song from './Song';
import Songs from './Songs';

export default class SongsManager {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _availableSongs: Song[];

  private _notes: number[];

  private _songsDb: Songs;

  private _activeSong: Song;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(songsDb: Songs) {
    this._songsDb = songsDb;
    this._notes = [];
    this._availableSongs = [];
  }

  // ----------------------------------------------------------------------------------------------
  next(): number {
    const note = this._notes.shift();
    if (note !== undefined) {
      return (note);
    }

    this.setSong();
    return (this._notes.shift());
  }

  // ----------------------------------------------------------------------------------------------
  setSong(): void {
    this._activeSong = Phaser.ArrayUtils.removeRandomItem(this._availableSongs);
    
    if (this._activeSong) {
      this._activeSong.remix();
    } else {
      if (!this._availableSongs.length) {
        this._availableSongs = this.getAvailableSongs();
        this._activeSong = Phaser.ArrayUtils.removeRandomItem(this._availableSongs);
      }

      if (this._activeSong) {
        this._activeSong.remix();
      } else {
        // last-ditch effore to get a song
        this._activeSong = new Song();
        const MAX_NOTES = 16;
        this._activeSong.setRandomNotes(MAX_NOTES);
      }
    }

    this._notes = this._notes.concat(this._activeSong.notes.concat());
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private getAvailableSongs(): Song[] {
    if (!this._songsDb) {
      return (null);
    }

    const songs: Song[] = [];
    
    this._songsDb.data.forEach((song) => {
      if (!song.skip) {
        songs.push(song);
      }
    });

    return (songs);
  }
}
