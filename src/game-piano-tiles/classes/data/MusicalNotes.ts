export default class MusicalNotes {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly SOUND_KEYS = [
    'piano-001',
    'piano-002',
    'piano-003',
    'piano-004',
    'piano-005',
    'piano-006',
    'piano-007',
    'piano-008',
    'piano-009',
    'piano-010',
    'piano-011',
    'piano-012',
    'piano-013',
  ];
}
