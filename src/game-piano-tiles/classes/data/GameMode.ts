export default class GameMode {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  /**
   * in this game mode, you clear a set number of tiles to add more time onto the timer. The max
   * number of tiles is predetermined.
   * Avoid pressing on the trap tiles.
   */
  static readonly ENDURANCE = 'endurance';

  /**
   * In this game mode, you clear as many tiles as you can within a set time frame. As soon as you
   * clear a tile, a new one will appear. The max number of tiles shown slowly increases over time.
   * Avoid pressing on the trap tiles.
   */
  static readonly FRENZY = 'frenzy';

  /**
   * In this game mode, you clear a predetermined number of random tiles by pressing them.
   * Once they're all clear, a new set of random tiles will apear. Clear as many patterns within
   * a set time frame.
   * Avoid pressing on the trap tiles.
   */
  static readonly PATTERN = 'pattern';
};
