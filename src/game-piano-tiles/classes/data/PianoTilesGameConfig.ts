import IPianoTilesGameConfig from '../../IPianoTilesGameConfig'

// ================================================================================================
export default function CreateConfigData(data?: IPianoTilesGameConfig): IPianoTilesGameConfig {
  if (!data) {
    data = {
      // place any custom config properties here.
      // see IPianoTilesGameConfig.ts for details on all valid properties.
    };  
  }

  return (data);
}
