import { getProperty } from '../../util/util';
import MusicalNotes from './MusicalNotes';

export default class Song {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  notes: number[];

  private _id: string;

  private _skip: boolean;

  private _jData: any;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(jData?: any) {
    this._jData = jData;
    this._id = getProperty(this._jData, 'id', '');
    this._skip = getProperty(this._jData, 'skip', false);
    this.remix();
  }

  // ----------------------------------------------------------------------------------------------
  get id(): string {
    return (this._id);
  }

  // ----------------------------------------------------------------------------------------------
  remix(): void {
    this.notes = Song.ParseNotes(getProperty(this._jData, 'notes', []));
  }

  // ----------------------------------------------------------------------------------------------
  setRandomNotes(numNotes: number): void {
    this.notes = Song.CreateRandomNotes(numNotes);
  }

  // ----------------------------------------------------------------------------------------------
  get skip(): boolean {
    return (this._skip);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private static CreateRandomNotes(numNotes: number): number[] {
    const MAX_NOTES = MusicalNotes.SOUND_KEYS.length;
    const out: number[] = [];

    for (let index = 0; index < numNotes; index += 1) {
      const note = Math.floor(Math.random() * MAX_NOTES);
      out.push(note);
    }
    return (out);
  }

  // ----------------------------------------------------------------------------------------------
  private static ParseNotes(notes: number[]): number[] {
    if (!notes || !notes.length) {
      const NUM_NOTES = 16;
      return (Song.CreateRandomNotes(NUM_NOTES));
    }

    let out: number[] = [];

    notes.forEach((unit) => {
      if (typeof(unit) === 'number') {
        out.push(unit);
      } else if (Array.isArray(unit)) {
        out = out.concat(Song.ParseRandomNote(unit));
      }
    });

    return (out);
  }

  // ----------------------------------------------------------------------------------------------
  private static ParseRandomNote(notes: number[]): number {
    const unit = Phaser.ArrayUtils.getRandomItem(notes);
    if (typeof(unit) === 'number') {
      return (unit);
    } else if (Array.isArray(unit)) {
      return (Song.ParseRandomNote(unit));
    }

    // last-ditch effort
    return (Math.floor(Math.random() * MusicalNotes.SOUND_KEYS.length));
  }
}