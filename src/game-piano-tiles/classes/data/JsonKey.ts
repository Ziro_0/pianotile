enum JsonKey {
  GRAPHICS_IMAGES = 'images',
  SONGS = 'songs',
  DIALOG_TEXT_KEY = 'fredoka_one',
  PIANO_FAIL = 'piano-fail',
  PIANO_MISS_0 = 'piano-miss-0',
  PIANO_MISS_1 = 'piano-miss-1',
  PIANO_MISS_2 = 'piano-miss-2',
};

export default JsonKey;
