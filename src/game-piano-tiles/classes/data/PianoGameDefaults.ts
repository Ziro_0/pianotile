import GameMode from './GameMode';

export default class PianoGameDefaults {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly ENDURANCE_BONUS_TIME = 9;

  static readonly ENDURANCE_NUM_TILES = 7;

  static readonly ENDURANCE_NUM_TILES_TIME_BONUS = 45;

  static readonly ENDURANCE_TIME = 45;

  static readonly FRENZY_INCR_MAX_TILES_RATE = 3.0;

  static readonly FRENZY_NUM_MAX_TILES = 6;

  static readonly FRENZY_NUM_START_TILES = 3;

  static readonly FRENZY_TIME = 45;

  static readonly GAME_MODE = GameMode.FRENZY;

  static readonly LOST_LIFE_NUM_FLASHES = 1;

  static readonly LOST_LAST_LIFE_NUM_FLASHES = 5;

  static readonly PATTERN_NUM_TILES = 4;

  static readonly PATTERN_TIME = 45;

  static readonly SHOULD_SCREEN_SHAKE = true;

  static readonly SHOW_TUTORIAL = false;
}
