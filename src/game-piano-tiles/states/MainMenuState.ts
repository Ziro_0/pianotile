export default class MainMenuState {
    game: any;
    constructor(game) {
        this.game = game;
    }

    create() {
        console.log('MainMenu state [create] started');
        this.game.state.start('GameState');
    }
}
