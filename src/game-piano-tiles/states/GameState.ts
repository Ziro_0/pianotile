import AudioPlayer from '../classes/audio/AudioPlayer';
import GameMode from '../classes/data/GameMode';
import JsonKey from '../classes/data/JsonKey';
import MusicalNotes from '../classes/data/MusicalNotes';
import pianoConfigValidate from '../classes/data/pianoConfigValidate';
import Songs from '../classes/data/Songs';
import SongsManager from '../classes/data/SongsManager';
import LostLivesHeart from '../classes/ui/LostLivesHeart';
import ModalDialog from '../classes/ui/ModalDialog';
import Tile from '../classes/ui/Tile';
import Tiles from '../classes/ui/Tiles';
import UiFlash from '../classes/ui/UiFlash';
import { Game } from '../game';
import EnduranceGameModule from '../gameModeModules/EnduranceGameModule';
import FrenzyGameModule from '../gameModeModules/FrenzyGameModule';
import GameModule from '../gameModeModules/GameModule';
import PatternsGameModule from '../gameModeModules/PatternsGameModule';
import Listeners from '../Listeners';

export default class GameState extends Phaser.State {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _tiles: Tiles;

  private _songsManager: SongsManager;

  private _audioPlayer: AudioPlayer;

  private _dialog: ModalDialog;

  private _gameModule: GameModule;

  private _backgroundGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _incorrectOrientationImage: Phaser.Group;

  private _timer: Phaser.Timer;

  private _lives: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    super();
  }

  // ----------------------------------------------------------------------------------------------
  get audio(): AudioPlayer {
    return (this._audioPlayer);
  }

  // ----------------------------------------------------------------------------------------------
  create() {
    console.log('Game state [create] started');

    this.gameObject.pianoTilesConfig = pianoConfigValidate(this.gameObject.pianoTilesConfig);

    this.game.stage.backgroundColor = '#000000';

    setTimeout(() => {
      this.initOrientation();

      this.initTimer();
      this.initGroups();
      this.initAudioPlayer();
      this.initBackground();
      this.initUi();
      this.initGameModule();
  
      this.getSongs();
  
      this.setupGameBoard();
      
      if (this.shouldShowInstructions()) {
        this.presentInstructions();
      } else {
        this.startGame();
      }
  
      this.listenerCallback(Listeners.READY, this.game);
        
    }, 10);
  }

  // ----------------------------------------------------------------------------------------------
  get gameObject(): Game {
    return (<Game>this.game);
  }

  // ----------------------------------------------------------------------------------------------
  listenerCallback(key: string, ...args): void {
    const callback: Function = this.gameObject.listenerMapping[key];
    if (callback) {
      callback.call(this.game, ...args);
    }
  }

  // ----------------------------------------------------------------------------------------------
  shutdown(): void {
    this._gameModule.onShutdown();
    this.audio.dispose();
    this.game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get tiles(): Tiles {
    return (this._tiles);
  }

  // ----------------------------------------------------------------------------------------------
  get timer(): Phaser.Timer {
    return (this._timer);
  }

  // ----------------------------------------------------------------------------------------------
  get uiGroup(): Phaser.Group {
    return (this._uiGroup);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private calcViewOffset(): number {
    const headerBarDiv = document.getElementById('headerBar');
    return (headerBarDiv ? headerBarDiv.clientHeight * this.game.scale.scaleFactor.y : 0);
  }

  // ----------------------------------------------------------------------------------------------
  private getSongs(): Songs {
    const jData = this.game.cache.getJSON(JsonKey.SONGS);
    if (!jData) {
      console.warn(`Songs JSON "${JsonKey.SONGS}" not found in cache.`)
    }

    return (new Songs(jData));
  }

  // ----------------------------------------------------------------------------------------------
  private handleCorrectTile(): void {
    const note = this._songsManager.next();
    const soundKey = MusicalNotes.SOUND_KEYS[note];
    this.audio.play(soundKey);
    this.listenerCallback(Listeners.ADD_POINT, 1);
  }

  // ----------------------------------------------------------------------------------------------
  private handleIncorrectTile(tile: Tile): void {
    this.livesLost(tile);
  }

  // ----------------------------------------------------------------------------------------------
  private hideIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      this._incorrectOrientationImage.destroy();
      this._incorrectOrientationImage = null;
    }

    this.game.paused = false;
  }

  // ----------------------------------------------------------------------------------------------
  private initAudioPlayer(): void {
    this._audioPlayer = new AudioPlayer(this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private initBackground(): void {
    const viewOffset = this.calcViewOffset()
    const image = this.add.image(0, viewOffset, JsonKey.GRAPHICS_IMAGES, 'game_bg_portrait',
      this._backgroundGroup);

    if (image.width < this.game.width) {
      image.width = this.game.width;
      image.scale.y = image.scale.x;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private initOrientation(): void {
    this.scale.onOrientationChange.add((scale: Phaser.ScaleManager,
      prevOrientation: string, wasIncorrect: boolean) => {
        const hasChanged = scale.screenOrientation !== prevOrientation;
        if (!hasChanged) {
          return;
        }

        const isIncorrect = scale.incorrectOrientation && !wasIncorrect;
        if (isIncorrect) {
          this.showIncorrectOrientationMessage();
        } else {
          this.hideIncorrectOrientationMessage();
        }
      });
  }

  // ----------------------------------------------------------------------------------------------
  private initGroup(): Phaser.Group {
    const group = this.game.add.group();
    return (group);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroups(): void {
    this._backgroundGroup = this.initGroup();
    this._uiGroup = this.initGroup();
  }

  // ----------------------------------------------------------------------------------------------
  private initTiles(): void {
    this._tiles = new Tiles(this.game);

    this._tiles.onTileDown.add(this.onTileDown, this);
    this._tiles.onTileComplete.add(this.onTileComplete, this);

    const logicalScale = this._tiles.width / 540;
    const width = this.game.scale.width * logicalScale;
    this._tiles.width = width;
    this._tiles.scale.y = this._tiles.scale.x;

    this._tiles.x = (this.game.scale.width - this._tiles.width) / 2;
    this._tiles.y = (this.game.scale.height - this._tiles.height) / 2;

    this.uiGroup.add(this._tiles);
  }

  // ----------------------------------------------------------------------------------------------
  private initTimer(): void {
    this._timer = this.game.time.create(false);
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private initUi(): void {
    this.initTiles();
  }

  // ----------------------------------------------------------------------------------------------
  private livesLost(tile: Tile): void {
    this.presentLostLivesHeart(tile.centerX, tile.centerY);

    this._lives -= 1;

    if (this._lives <= 0) {
      this.handleLostLastLife();
    } else {
      this.handleLostOneLife();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private handleLostLastLife(): void {
    this.audio.play(JsonKey.PIANO_FAIL);

    this.presentUiFlash(this.gameObject.pianoTilesConfig.lostLastLifeNumFlashes);
    this.presentScreenTremor();

    this.timer.add(1500,
      () => {
        this.notifyLivesLost();
      }, this);
  }

  // ----------------------------------------------------------------------------------------------
  private handleLostOneLife(): void {
    const keys = [ JsonKey.PIANO_MISS_0, JsonKey.PIANO_MISS_1, JsonKey.PIANO_MISS_2 ];
    this.audio.play(keys);

    this.presentUiFlash(this.gameObject.pianoTilesConfig.lostLifeNumFlashes);

    this.notifyLivesLost();
  }

  // ----------------------------------------------------------------------------------------------
  private notifyLivesLost(): void {
    this.listenerCallback(Listeners.LIVES_LOST, this.gameObject);
  }

  // ----------------------------------------------------------------------------------------------
  private onTileComplete(tile: Tile): void {
    this._gameModule.onTileComplete(tile);
  }

  // ----------------------------------------------------------------------------------------------
  private presentInstructions(): void {
    const message = this._gameModule.onGetInstructionMessage();
    const logicalScale = this._tiles.width / 540;
    console.log('logical scale: ', logicalScale)
    const FONT_SIZE = 72 * logicalScale;
    this.listenerCallback(Listeners.TUTORIAL_OPEN);
    this.presentModalDialog(message,
      () => {
        this.listenerCallback(Listeners.TUTORIAL_CLOSE);
        this.startGame();
      },
      FONT_SIZE);
  }

  // ----------------------------------------------------------------------------------------------
  private presentLostLivesHeart(x: number, y: number): void {
    x += this.tiles.x;
    y += this.tiles.y;
    const heart = new LostLivesHeart(this.game, x, y, -1);
    this.uiGroup.add(heart);
  }

  // ----------------------------------------------------------------------------------------------
  private presentModalDialog(message: string | string[], callback: Function, fontSize?: number,
    context?: any): void {
    if (!this._dialog) {
      this._dialog = new ModalDialog(this.game, this.uiGroup);
    }

    this._dialog.onComplete.removeAll();
    this._dialog.onComplete.addOnce(callback, context || this);
    this._dialog.present(message, fontSize);
  }
  
  // ----------------------------------------------------------------------------------------------
  private presentScreenTremor(): void {
    const config = this.gameObject.pianoTilesConfig;
    
    if (!config.shouldScreenShake) {
      return;
    }

    const INTENSITY = 0.02;
    const DURATION = config.lostLastLifeNumFlashes *
      (UiFlash.DEFAULT_FLASH_ON_DURATION + UiFlash.DEFAULT_FLASH_OFF_DURATION);
    this.camera.shake(INTENSITY, DURATION);
  }

  // ----------------------------------------------------------------------------------------------
  private presentUiFlash(numFlashes: number): void {
    const uiFlash = new UiFlash(this.game, numFlashes);
    this.uiGroup.add(uiFlash);
  }

  // ----------------------------------------------------------------------------------------------
  private onTileDown(tile: Tile, isCorrect: boolean): void {
    if (isCorrect) {
      this.handleCorrectTile();
    } else {
      this.handleIncorrectTile(tile);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private setupGameBoard(): void {
    this._songsManager = new SongsManager(this.getSongs());
    this.setSong();
    this._gameModule.setupBoard();
  }

  // ----------------------------------------------------------------------------------------------
  private initGameModule(): void {
    const gameMode = this.gameObject.pianoTilesConfig.gameMode;
    switch (gameMode) {
      case GameMode.ENDURANCE:
        this._gameModule = new EnduranceGameModule(this);
        break;

      case GameMode.FRENZY:
        this._gameModule = new FrenzyGameModule(this);
        break;

      case GameMode.PATTERN:
      default:
        this._gameModule = new PatternsGameModule(this);
        break;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private setSong(): void {
    this._songsManager.setSong();
  }

  // ----------------------------------------------------------------------------------------------
  private showIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      return;
    }

    this.game.paused = true;
    this._incorrectOrientationImage = this.add.group();

    const graphics = this.add.graphics(0, 0, this._incorrectOrientationImage);
    graphics.beginFill(0, 1.0);
    graphics.drawRect(0, 0, this.game.width, this.game.height);
    graphics.endFill();

    const image = this.add.image(0, 0, JsonKey.GRAPHICS_IMAGES, 'incorrect-orientation-message',
      this._incorrectOrientationImage);
    image.anchor.set(0.5);
    image.x = this.game.width / 2;
    image.y = this.game.height / 2;

    image.width = this.game.width;
    image.scale.y = image.scale.x;
    
    if (image.height < this.game.height) {
      image.height = this.game.height
      image.scale.x = image.scale.y;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private shouldShowInstructions(): boolean {
    return (this.gameObject.pianoTilesConfig.showTutorial === true);
  }

  // ----------------------------------------------------------------------------------------------
  private startGame(): void {
    const startTime = this._gameModule.getStartTime();
    this._lives = this.gameObject.startLives;
    this.listenerCallback(Listeners.SET_SECONDS, startTime);
    this._gameModule.start();
  }
}
