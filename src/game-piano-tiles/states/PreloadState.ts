export default class PreloadState {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly BASE_PATH = 'assets/game-piano-tiles/';

  private game: Phaser.Game;
  
  private preloadBar: Phaser.Image;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this.game = game;
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    this.game.state.start('MainMenuState');
  }

  // ----------------------------------------------------------------------------------------------
  preload(): void {
    this.preloadBar = this.game.add.image(
      this.game.world.centerX,
      this.game.world.centerY,
      'preload_bar');
    this.preloadBar.anchor.set(0.5);

    this.game.load.setPreloadSprite(this.preloadBar);

    // load assets
    this.loadAtlases();
    this.loadJsonFiles();
    this.loadSounds();
    this.loadFonts();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private loadAtlas(key: string): void {
    const baseUrl = `${PreloadState.BASE_PATH}atlases/${key}`;
    this.game.load.atlas(key, `${baseUrl}.png`, `${baseUrl}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadAtlases(): void {
    [
      'images'
    ].forEach((key) => {
      this.loadAtlas(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadFont(key: string): void {
    const path = `${PreloadState.BASE_PATH}fonts/`;
    this.game.load.bitmapFont(key, `${path}${key}.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFontBfg(key: string): void {
    // Loads fonts created by Bitmap Font Generator. BFG appends a "_x"-like template onto the 
    // ends of its image files, which doesn't exactly match key (which doesn't use the template).
    const path = `${PreloadState.BASE_PATH}fonts/`;
    this.game.load.bitmapFont(key, `${path}${key}_0.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFonts(): void {
    [
    ].forEach((key) => {
      this.loadFont(key);
    });

    [
      'fredoka_one'
    ].forEach((key) => {
      this.loadFontBfg(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadJson(key: string): void {
    const path = `${PreloadState.BASE_PATH}json/`;
    this.game.load.json(key, `${path}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadJsonFiles(): void {
    [
      'songs'
    ].forEach((key) => {
      this.loadJson(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSound(key: string): void {
    const baseUrl = `${PreloadState.BASE_PATH}sounds/${key}`;
    const mp3Url = `${baseUrl}.mp3`;
    const oggUrl = `${baseUrl}.ogg`;
    const urls = [
      mp3Url,
      oggUrl
    ];

    this.game.load.audio(key, urls);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSounds(): void {
    [
      'piano-001',
      'piano-002',
      'piano-003',
      'piano-004',
      'piano-005',
      'piano-006',
      'piano-007',
      'piano-008',
      'piano-009',
      'piano-010',
      'piano-011',
      'piano-012',
      'piano-013',
      'piano-miss-0',
      'piano-miss-1',
      'piano-miss-2',
      'piano-fail',
    ].forEach((key) => {
      this.loadSound(key);
    });
  }
}