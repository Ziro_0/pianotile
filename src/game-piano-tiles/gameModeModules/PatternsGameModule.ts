import Tile from '../classes/ui/Tile';
import GameModule from './GameModule';

export default class PatternsGameModule extends GameModule {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _numTargetsRemaining: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  getStartTime(): number {
    return (this.config.patternTime);
  }

  // ----------------------------------------------------------------------------------------------
  onGetInstructionMessage(): string | string[] {
    return ([
      'Clear all patterns by tapping all black tiles',
      'Don\'t tap the white tiles',
    ]);
  }

  // ----------------------------------------------------------------------------------------------
  onTileComplete(tile: Tile): void {
    this._numTargetsRemaining -= 1;
    if (this._numTargetsRemaining <= 0) {
      this.setupBoard();
    }
  }

  // ----------------------------------------------------------------------------------------------
  setupBoard(): void {
    this._numTargetsRemaining = this.config.patternNumTiles;
    this.state.tiles.setRandomTilesAsTargets(this._numTargetsRemaining);
  }
}