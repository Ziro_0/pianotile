import Tile from '../classes/ui/Tile';
import { removeTimer } from '../util/util';
import GameModule from './GameModule';

export default class FrenzyGameModule extends GameModule {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _maxTilesTimerEvent: Phaser.TimerEvent;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  getStartTime(): number {
    return (this.config.frenzyTime);
  }

  // ----------------------------------------------------------------------------------------------
  onGetInstructionMessage(): string | string[] {
    return (
      [
        'Tap as many tiles as you can in before the time\'s up',
        'Don\'t tap the white tiles',
      ]
    );
  }

  // ----------------------------------------------------------------------------------------------
  onShutdown(): void {
    this._maxTilesTimerEvent = removeTimer(this._maxTilesTimerEvent);
  }

  // ----------------------------------------------------------------------------------------------
  onTileComplete(tile: Tile): void {
    this.state.tiles.setRandomTilesAsTargets(1);
  }

  // ----------------------------------------------------------------------------------------------
  setupBoard(): void {
    const numStartTiles = this.config.frenzyNumStartTiles;
    this.state.tiles.setRandomTilesAsTargets(numStartTiles);
  }

  // ----------------------------------------------------------------------------------------------
  start(): void {
    this.createMaxTilesTimer();
  }

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private createMaxTilesTimer(): void {
    const rate = this.config.frenzyIncrMaxTileRate * 1000;
    if (rate <= 0) {
      return;
    }

    const repeat = this.config.frenzyNumMaxTiles - this.config.frenzyNumStartTiles;
    if (repeat >= 0) {
      this._maxTilesTimerEvent = this.state.timer.repeat(rate, repeat, this.onMaxTilesTimer, this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onMaxTilesTimer(): void {
    this.state.tiles.setRandomTilesAsTargets(1);
  }
}