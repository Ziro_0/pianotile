import Tile from '../classes/ui/Tile';
import Listeners from '../Listeners';
import GameModule from './GameModule';

export default class EnduranceGameModule extends GameModule {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _numTargetsRemaining: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  getStartTime(): number {
    return (this.config.enduranceTime);
  }

  // ----------------------------------------------------------------------------------------------
  onGetInstructionMessage(): string | string[] {
    const numTiles = this.config.enduranceNumTilesTimeBonus;
    const timeBonus = this.config.enduranceBonusTime;

    return ([
      `Tap ${numTiles} black tiles to get +${timeBonus} seconds`,
      'Don\'t tap the white tiles',
    ]);
  }

  // ----------------------------------------------------------------------------------------------
  onTileComplete(tile: Tile): void {
    this._numTargetsRemaining -= 1;
    
    if (this._numTargetsRemaining <= 0) {
      this._numTargetsRemaining = this.config.enduranceNumTilesTimeBonus;

      const offset = true;
      console.log(this.config.enduranceBonusTime)
      this.state.listenerCallback(Listeners.SET_SECONDS, this.config.enduranceBonusTime, offset);
    }

    this.state.tiles.setRandomTilesAsTargets(1);
  }

  // ----------------------------------------------------------------------------------------------
  setupBoard(): void {
    this._numTargetsRemaining = this.config.enduranceNumTilesTimeBonus;
    this.state.tiles.setRandomTilesAsTargets(this.config.enduranceNumTiles);
  }
}