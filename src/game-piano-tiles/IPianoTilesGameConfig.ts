import GameMode from './classes/data/GameMode';

// ================================================================================================
export default interface IPianoTilesGameConfig {
  /**
   * In Endurance mode, the number of seconds added to the timer for clearing to many tiles.
   */
  enduranceBonusTime?: number;

  /**
   * In Endurance mode, the number of tiles that will appear.
   */
  enduranceNumTiles?: number;

  /**
   * In Endurance mode, this is the number of tiles you must clear to gain more time.
   */
  enduranceNumTilesTimeBonus?: number;

  /**
   * Starting time, in seconds of an Endurance mode game.
   */
  enduranceTime?: number;

  /**
   * In Frenzy mode, this specifies the rate, in seconds, at which the max number of target
   * tiles increases.
   */
  frenzyIncrMaxTileRate?: number;

   /**
   * In Frenzy mode, the threshold of the maxiumu number of target tiles that available.
   */
  frenzyNumMaxTiles?: number;

  /**
   * In Frenzy mode, the number of target tiles that are available at the start of the game.
   */
  frenzyNumStartTiles?: number;

  /**
   * Starting time, in seconds of a Frenzy mode game.
   */
  frenzyTime?: number;

  /**
   * Type of game to play. See `GameMode` for more details.
   */
  gameMode?: GameMode;

  /**
   * Number of times the screen flashes when the player loses a life.
   */
  lostLifeNumFlashes?: number;

  /**
   * Number of times the screen flashes when the player loses their last life.
   */
  lostLastLifeNumFlashes?: number;

  /**
   * In Pattern mode, the number of tiles that will appear in each set.
   */
  patternNumTiles?: number;

  /**
   * In Pattern mode, this is the duration, in seconds, of a game sesion.
   */
  patternTime?: number;

  /**
   * If true, the phaser camera will shake when the last life is lost.
   */
  shouldScreenShake?: boolean;

  /**
   * If true, the game will show instructions, depending on the selected game mode.
   */
  showTutorial?: boolean;
}
